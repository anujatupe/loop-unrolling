#include <time.h>
#include <omp.h>
#include <cstdint>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

//#define N 1000
//#define M 1000
#define SECINNANOSEC 1000000000L

using namespace std;

//int data[N][M];

int modify_matrix(int **data, int N, int M) {
  int largeDim;
  int smallDim;
  if(N > M) {
    smallDim = M;
  largeDim = N;
  } else {
  smallDim = N;
  largeDim = M;
  }
  int diag;
  for (diag = 1; diag <= N+M-3; diag++) {
    int diaglength = diag;
    if ((diag + 1) >= smallDim) {
      diaglength = smallDim - 1;
    }
    if ((diag  +1) >= largeDim) {
      diaglength = (smallDim - 1) - (diag - largeDim) - 1;
    }

    int i, k, j;
    #pragma omp parallel shared (data, diag, diaglength, N, M) private (i, k, j) 
    {
      #pragma omp for schedule(static)
      for (k = 0; k < diaglength; k++) {
        i = diag - k;
        j = k + 1;
        if (diag > (N - 1)) {
          i = N - 1 - k;
          j = diag - (N - 1) + k +1;
        }
        data[i][j] = data[i-1][j] + data[i][j-1]; 
      }
    }
  

  }
  return 0;
}

int ** allocate_space_for_array(int m, int n) {
  int i;
  int **matrix;
  matrix = (int **) malloc(m * sizeof(int*)) ;
  for(i = 0; i<m; i++) {
    matrix[i] = (int *) malloc(n * sizeof(int));
  }
  return matrix;
}

void freeArray(int **matrix, int m) {
  int i;
  for (i = 0; i < m; ++i) {
    free(matrix[i]);
  }
  free(matrix);
}

void print_matrix(int **matrix, int m, int y) {
  int i, j;
  for (i = 0; i < m; i++) {
    cout<<"\n";
    for (j = 0; j < y; j++) {
      cout<<matrix[i][j]<<"\t";
    }
  }
}

int main(int argc, char *argv[]) {
  int **data;
  timespec start_time, end_time;
  uint64_t diff_time;
  int row, col;
  int N, M;
  
  //cout<<"\nEnter the number of rows and columns in matrix:";
  //cin>>N>>M;
  N = atoi(argv[1]);
  M = atoi(argv[2]);  

  data = allocate_space_for_array(N, M);
 //initialize the data matrix
  for(row = 0; row < N; row++) {
  	for (col = 0; col < M; col++) {
  		data[row][col] = rand() % 10 + 1;
  	}
  }

  //cout<<"\nThis is the input matrix:\n";
  //print_matrix(data, N, M);
  // cout<<"\n";

  clock_gettime(CLOCK_MONOTONIC, &start_time);
  modify_matrix(data, N, M);
  clock_gettime(CLOCK_MONOTONIC, &end_time);

  diff_time = SECINNANOSEC * (end_time.tv_sec - start_time.tv_sec) + end_time.tv_nsec - start_time.tv_nsec;
  cout<<"\n\n*** Time required parallel matrix multiplication: "<<(long long unsigned int) diff_time<<" nanoseconds ***\n\n";

  //cout<<"\nThis is the modified matrix:\n";
  //print_matrix(data, N, M);   

  freeArray(data, N);
  return 0;	
}
